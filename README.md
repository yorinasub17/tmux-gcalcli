# tmux-gcalcli

This is a tmux plugin that uses gcalcli to allow you to show the upcoming
calendar event in your google calendar on the tmux status line.

This plugin was inspired by [Richard Johnson's blog post on doing the same for
byobu](https://www.nixternal.com/byobu-shows-me-next-meeting/).

## Installation

- install and setup [gcalcli](https://github.com/insanum/gcalcli)
- install the following crontab
```
*/5 * * * * /usr/local/bin/gcalcli --tsv --nocolor agenda "`date`" > $HOME/.gcal_agenda.txt && echo "gcalcli ran at $(date)" >> $HOME/.logs/gcalcli_cron.log
```
- setup plugin in tmux conf


