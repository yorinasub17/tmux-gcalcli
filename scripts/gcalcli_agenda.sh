#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

GCAL=$(awk -f $CURRENT_DIR/gcalcli_compact_agenda.awk $HOME/.gcal_agenda.txt)
echo "${GCAL:0:20}"
