BEGIN {
    FS="\t";
    OFS=" ";
    "date +'%Y'"|getline y;
}

FNR == 1 {
    if(y == substr($1, 0, 4))
    {
        print substr($1, 6), $2, $5
    }
    else
    {
        print $1, $2, $5
    }
}
